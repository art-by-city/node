import ArtByCityNode from '../../src/app'

export const node =
  new ArtByCityNode('MlV6DeOtRmakDOf6vgOBlif795tcWimgyPsYYNQ8q1Y')

before(() => {
  node.start()
})

after(async () => {
  await node.stop()
})
