import 'reflect-metadata'
import chai from 'chai'
import chaiHttp from 'chai-http'

import { node } from './setup'

chai.use(chaiHttp)
const expect = chai.expect

describe('Art By City Node', () => {
  it('should 307 redirect unmatched routes to gateway', async () => {
    const route = '/tx/mock-tx-id'

    try {
      const res = await chai.request(node.server).get(route).redirects(0)

      expect(res).to.have.status(307)
      expect(res.header.location).to.equal(`http://localhost:1984${route}`)
    } catch (err) {
      expect.fail(err)
    }
  })
})
