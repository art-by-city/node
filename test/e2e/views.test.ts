import 'reflect-metadata'
import chai from 'chai'
import chaiHttp from 'chai-http'

import { node } from './setup'

chai.use(chaiHttp)
const expect = chai.expect

describe('Views API', () => {
  it('/:txid should redirect 302 to gateway', async () => {
    const route = '/mock-tx-id'

    try {
      const res = await chai.request(node.server).get(route).redirects(0)

      expect(res).to.have.status(302)
      expect(res.header.location).to.equal(`http://localhost:1984${route}`)
    } catch (err) {
      expect.fail(err)
    }
  })

  it('/views/:txid should return view count', async () => {
    const route = '/views/mock-tx-id'

    try {
      const res = await chai.request(node.server).get(route)

      expect(res).to.have.status(200)
      // NB: chai-http converts 0 to {}
      // check for empty object and convert to 0, otherwise use res.body
      const views = typeof res.body === 'object'
        && Object.keys(res.body).length === 0
          ? 0 : res.body
      expect(views).to.be.a('number')
    } catch (err) {
      expect.fail(err)
    }
  })

  it('/views/report/:date should return view report for date', async () => {
    const date = '1-1-1969'
    const route = `/views/report/${date}`

    try {
      const res = await chai.request(node.server).get(route)

      expect(res).to.have.status(200)
      expect(res).to.be.json
      expect(res.body.date).to.equal(date)
      expect(res.body.views).to.be.an('array')
    } catch (err) {
      expect.fail(err)
    }
  })

  it('/views/trending/:date should return trend report for date', async () => {
    const date = '1-1-1969'
    const route = `/views/trending/${date}`

    try {
      const res = await chai.request(node.server).get(route)

      expect(res).to.have.status(200)
      expect(res).to.be.json
      expect(res.body.date).to.equal(date)
      expect(res.body.rankings).to.be.an('array')
    } catch (err) {
      expect.fail(err)
    }
  })
})
