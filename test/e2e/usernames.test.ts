import 'reflect-metadata'
import chai from 'chai'
import chaiHttp from 'chai-http'

import { node } from './setup'

chai.use(chaiHttp)
const expect = chai.expect

describe('Stats API', () => {
  it('/usernames should return usernames contract state', async () => {
    const route = '/usernames'

    try {
      const res = await chai.request(node.server).get(route)

      expect(res).to.have.status(200)
      expect(res).to.be.json
    } catch (err) {
      expect.fail(err)
    }
  })
})
