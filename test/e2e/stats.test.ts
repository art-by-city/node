import 'reflect-metadata'
import chai from 'chai'
import chaiHttp from 'chai-http'

import { node } from './setup'

chai.use(chaiHttp)
const expect = chai.expect

describe('Stats API', () => {
  it('/stats/summary should return dapp stats summary', async () => {
    const route = '/stats/summary'

    try {
      const res = await chai.request(node.server).get(route)

      expect(res).to.have.status(200)
      expect(res.body).to.not.be.empty
    } catch (err) {
      expect.fail(err)
    }
  })
})
