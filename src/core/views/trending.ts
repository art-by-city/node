export type Ranking = { txid: string, views: number, type: string }

export class TrendingReport {
  date!: string
  rankings!: Ranking[]

  constructor(date: string, rankings: Ranking[]) {
    this.date = date
    this.rankings = rankings
  }

  toJSON() {
    return {
      date: this.date,
      rankings: this.rankings
    }
  }
}
