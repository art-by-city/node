import { createHash } from 'crypto'

export class View {
  node!: string
  txid!: string
  hash!: string
  type!: string
  date!: string
  timestamp!: string

  constructor(opts: {
    nodeAddress: string,
    txid: string,
    type: string,
    ip: string,
    ipIsHashed?: boolean,
    date?: string,
    timestamp?: string
  }) {
    this.node = opts.nodeAddress
    this.txid = opts.txid
    this.type = opts.type.split(';')[0]
    this.timestamp = opts.timestamp || new Date().toISOString()

    if (opts.date) {
      this.date = opts.date
    } else {
      this.date = new Date(this.timestamp)
        .toLocaleDateString()
        .replace(/\//g, '-')
    }

    if (!opts.ipIsHashed) {
      this.hash = createHash('sha256')
        .update(opts.date + opts.nodeAddress + opts.ip + opts.txid)
        .digest('hex')
    } else {
      this.hash = opts.ip
    }
  }
}
