import { View } from './'

export class ViewReport {
  date!: string
  views!: View[]

  constructor(date: string, views: View[]) {
    this.date = date
    this.views = views
  }

  toJSON() {
    return {
      date: this.date,
      views: this.views
    }
  }
}
