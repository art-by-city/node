import fs from 'fs'
import ArtByCityNode from './app'

(async () => {
  const adminJWKPath = process.env.ADMIN_JWK_PATH || 'ADMIN_JWK_PATH not set!'
  const adminJWKBuffer = fs.readFileSync(adminJWKPath)
  const adminJWK = JSON.parse(adminJWKBuffer.toString())
  const abc = new ArtByCityNode(adminJWK)

  await abc.start()

  process.on('SIGINT', () => { abc.stop() })
  process.on('SIGTERM', () => { abc.stop() })
})()
