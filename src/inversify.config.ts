import 'reflect-metadata'
import { Container } from 'inversify'
import { Knex } from 'knex'

import { DatabaseAdapter, KnexAdapter } from './infra'

const container = new Container()
container
  .bind<DatabaseAdapter<Knex>>(Symbol.for('KnexAdapter'))
  .to(KnexAdapter)

export { container }
