import 'dotenv/config'
import Koa from 'koa'
import Router from '@koa/router'
import bodyParser from 'koa-bodyparser'
import { Knex } from 'knex'
import { Server } from 'http'
import Arweave from 'arweave'
import { JWKInterface } from 'arweave/node/lib/wallet'

import { container } from './inversify.config'
import { CONFIG } from './util'
import {
  IdentityRouter,
  StatsRouter,
  UsernamesRouter,
  ViewsRouter
} from './interface'
import {
  AuthState,
  DatabaseAdapter,
  ExmAdapter,
  SmartweaveAdapter,
  StatsRepository,
  UsernamesContractCache,
  ViewsRepository
} from './infra'

export type State = Koa.DefaultState & {
  auth?: AuthState
}
export type Context = Koa.DefaultContext & {}
export type ParameterizedContext = Koa.ParameterizedContext<
  State,
  Context & Router.RouterParamContext<State, Context>,
  unknown
>

export default class ArtByCityNode {
  private port: number = 1985
  private db!: DatabaseAdapter<Knex>
  private nodeAddress!: string
  server!: Server
  app: Koa = new Koa()

  constructor(private adminJWK: JWKInterface) {
    this.setNodeAddress()
    this.build()
  }

  private build() {
    this.db = container
      .get<DatabaseAdapter<Knex>>(Symbol.for('KnexAdapter'))
    const viewsRepository = new ViewsRepository(this.db)
    const statsRepository = new StatsRepository(this.db)

    const smartweave = new SmartweaveAdapter()
    const usernamesCache = new UsernamesContractCache(smartweave.warp)

    const exm = new ExmAdapter(CONFIG.EXM_API_KEY)

    const viewsRouter = new ViewsRouter(
      this.nodeAddress,
      CONFIG.ABC_GATEWAY,
      viewsRepository
    )
    const statsRouter = new StatsRouter(statsRepository)
    const usernamesRouter = new UsernamesRouter(usernamesCache)
    const identityRouter = new IdentityRouter(exm)

    const router = new Router()
    router
      .use(
        '/stats',
        statsRouter.router.routes(),
        statsRouter.router.allowedMethods()
      )
      .use(
        '/usernames',
        usernamesRouter.router.routes(),
        usernamesRouter.router.allowedMethods()
      )
      .use(
        '/identity',
        identityRouter.router.routes(),
        identityRouter.router.allowedMethods()
      )
      // NB: views router must be last because it contains the /:txid endpoint!
      .use(
        viewsRouter.router.routes(),
        viewsRouter.router.allowedMethods()
      )

    this.app
      .use(async (ctx, next) => {
        ctx.set('Access-Control-Allow-Origin', '*')

        await next()
      })
      .use(bodyParser())
      .use(router.routes())
      .use(router.allowedMethods())
      .use(async (ctx) => {
        if (ctx.request.method === 'OPTIONS') {
          ctx.set('Access-Control-Allow-Headers', '*')
        } else {
          ctx.status = 307
          ctx.set('location', `${CONFIG.AR_GATEWAY}${ctx.request.url}`)
        }

        return
      })
  }

  private async setNodeAddress() {
    const arweave = new Arweave({})
    this.nodeAddress = await arweave.wallets.jwkToAddress(this.adminJWK)
  }

  async start() {
    if (!this.server) {
      this.server = this.app.listen(this.port, () => {
        console.log(`Art By City Node listening on ${this.port}`)
      })
    }
  }

  async stop() {
    if (this.db) {
      await this.db.connection.destroy()
    }

    if (this.server) {
      this.server.close(() => console.log('Art By City Node stopped'))
    }
  }
}
