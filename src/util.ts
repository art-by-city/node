const isProduction = process.env.NODE_ENV === 'production'
                     || process.env.NODE_ENV === 'staging'

const ABC_GATEWAY = isProduction
  ? process.env.APP_NAME === 'ArtByCity-Staging'
    ? 'https://staging.gateway.artby.city'
    : 'https://gateway.artby.city'
  : 'http://localhost:1984'

const AR_GATEWAY = isProduction
  ? 'https://arweave.net'
  : 'http://localhost:1984'

export const CONFIG = {
  GCP_PROJECT: process.env.GCP_PROJECT || 'art-by-city-dev',
  JOBS_COLLECTION: 'arweave-dapp-cacher-jobs',
  DB_HOST: process.env.DB_HOST || 'localhost',
  DB_PORT: Number.parseInt(process.env.DB_PORT || '8080'),
  DB_SSL: process.env.DB_SSL === 'true',
  DB_USER: process.env.DB_USER || undefined,
  DB_PASS: process.env.DB_PASS || undefined,
  DB_NAME: process.env.DB_NAME || undefined,
  DB_SCHEMA: process.env.DB_SCHEMA || 'dapp-cache',
  JOBS_SCHEMA_NAME: (process.env.DB_SCHEMA || 'dapp-cache') + '-jobs',
  AR_PROTOCOL: process.env.AR_PROTOCOL || 'http',
  AR_HOST: process.env.AR_HOST || 'localhost',
  AR_PORT: process.env.AR_PORT || 1984,
  ABC_GATEWAY: ABC_GATEWAY,
  AR_GATEWAY,
  AR_APP_NAME: process.env.AR_APP_NAME || 'ArtByCity-Development',
  AR_APP_VERSION: process.env.AR_APP_VERSION,
  PUBSUB_TOPIC: process.env.PUBSUB_TOPIC || 'cache-tx-request',
  BUCKET: process.env.BUCKET || 'gateway',
  STORAGE_URL: process.env.STORAGE_URL,
  PUBSUB_URL: process.env.PUBSUB_URL,
  QUEUE_LIMIT: Number.parseInt(process.env.QUEUE_LIMIT || '1'),
  DAPP_SCHEMA: process.env.DAPP_SCHEMA || 'artbycity',
  USERNAMES_CONTRACT_ID: process.env.USERNAMES_CONTRACT_ID || '',
  isProduction,
  EXM_API_KEY: process.env.EXM_API_KEY || '',
  ARK_EXM_FUNCTION_ID: process.env.ARK_EXM_FUNCTION_ID || ''
}
