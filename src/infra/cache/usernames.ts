import { Contract, Warp } from 'warp-contracts'

import { CONFIG } from '../../util'

export interface UsernamesContractState {
  usernames: {
    [owner: string]: string
  }
}

export class UsernamesContractCache {
  warp!: Warp
  contract!: Contract<UsernamesContractState>

  constructor(warp: Warp) {
    this.warp = warp
    this.contract = this.warp.contract(CONFIG.USERNAMES_CONTRACT_ID)
  }

  async get(): Promise<UsernamesContractState['usernames']> {
    const { cachedValue: { state } } = await this.contract.readState()

    return state.usernames
  }
}
