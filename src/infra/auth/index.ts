export type AuthState = {
  arweavePublicKey: string,
  jwt: string
}
