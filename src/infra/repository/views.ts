import { inject } from 'inversify'
import knex, { Knex } from 'knex'

import { TrendingReport, ViewReport, View, Ranking } from '../../core/views'
import { CONFIG } from '../../util'
import { DatabaseAdapter } from '../'

const COLLECTION_NAME = 'art-by-city-views4'
const TABLE_NAME = 'views'

export class ViewsRepository {
  private pool!: Knex

  constructor(
    @inject(Symbol.for('KnexAdapter')) adapter: DatabaseAdapter<Knex>
  ) {
    this.pool = adapter.connection
  }

  async insert(view: View): Promise<void> {
    try {
      const table = this.pool.withSchema(CONFIG.DAPP_SCHEMA).table(TABLE_NAME)
      await table.insert(view).onConflict().ignore()
    } catch (err) {
      console.error('Error inserting view', err)
    }
  }

  async count(txid: string): Promise<number> {
    const table = this.pool.withSchema(CONFIG.DAPP_SCHEMA).table(TABLE_NAME)
    const [{ count }] = await table.where({ txid }).count('hash')

    return count as number
  }

  async report(date: string): Promise<ViewReport> {
    const views = await this.fetchViewsForDate(date)

    return new ViewReport(date, views)
  }

  async trending(date: string): Promise<TrendingReport> {
    const views = await this.fetchViewsForDate(date)

    const rankings = Object.values<Ranking>(
      views.reduce(
        (counts, view) => {
          if (!counts[view.txid]) {
            counts[view.txid] = { txid: view.txid, type: view.type, views: 0 }
          }

          counts[view.txid].views += 1

          return counts
        },
        {} as any
      )
    ).sort((a, b) => b.views - a.views)

    return new TrendingReport(date, rankings)
  }

  private async fetchViewsForDate(date: string): Promise<View[]> {
    const table = this.pool.withSchema(CONFIG.DAPP_SCHEMA).table(TABLE_NAME)
    const views = await table.where({ date })

    return views
  }
}