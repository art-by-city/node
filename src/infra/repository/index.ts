import { View } from '../../core/views'

declare module 'knex/types/tables' {
  interface Tables {
    views: View
  }
}

export { StatsRepository } from './stats'
export { ViewsRepository } from './views'
