import { inject } from 'inversify'
import { Knex } from 'knex'

import { DatabaseAdapter } from '../'

export class StatsRepository {
  private pool!: Knex
  private schema = 'dapp-cache'

  constructor(
    @inject(Symbol.for('KnexAdapter')) adapter: DatabaseAdapter<Knex>
  ) {
    this.pool = adapter.connection
  }

  async summary(): Promise<any> {
    const schema = this.pool.withSchema(this.schema)
    const transactionsTable = schema.table('transactions')

    const usersQuery = transactionsTable.clone()
      .countDistinct('owner_address')
      .whereRaw('tags @\\? \'$[*].name \\? (@ == "App-Name")\'')
      .andWhereRaw('tags @\\? \'$[*].value \\? (@ == "ArtByCity")\'')
    const users = (await usersQuery)[0].count

    const dappUsersQuery = usersQuery.clone()
      .andWhereRaw('tags @\\? \'$[*].name \\? (@ == "App-Version")\'')
      .andWhereRaw('tags @\\? \'$[*].value \\? (@ == "0.0.1-alpha")\'')
    const dappUsers = (await dappUsersQuery)[0].count

    const artistsQuery = usersQuery.clone()
      .andWhereRaw('tags @\\? \'$[*].name \\? (@ == "Category")\'')
      .andWhereRaw('tags @\\? \'$[*].value \\? (@ == "artwork")\'')
    const artists = (await artistsQuery)[0].count

    const transactionsQuery = transactionsTable.clone().count('id')
    const transactions = (await transactionsQuery)[0].count

    const dappTransactionsQuery = transactionsQuery.clone()
      .whereRaw('tags @\\? \'$[*].name \\? (@ == "App-Name")\'')
      .andWhereRaw('tags @\\? \'$[*].value \\? (@ == "ArtByCity")\'')
      .andWhereRaw('tags @\\? \'$[*].name \\? (@ == "App-Version")\'')
      .andWhereRaw('tags @\\? \'$[*].value \\? (@ == "0.0.1-alpha")\'')
    const dappTransactions = (await dappTransactionsQuery)[0].count

    const publicationsQuery = dappTransactionsQuery.clone()
      .andWhereRaw('tags @\\? \'$[*].name \\? (@ == "Category")\'')
      .andWhereRaw('tags @\\? \'$[*].value \\? (@ == "artwork")\'')
      .andWhereRaw('tags @\\? \'$[*].name \\? (@ == "Content-Type")\'')
      .andWhereRaw('tags @\\? \'$[*].value \\? (@ == "application/json")\'')
    const publications = (await publicationsQuery)[0].count

    const likesQuery = transactionsTable.clone()
      .andWhereRaw('tags @\\? \'$[*].name \\? (@ == "Category")\'')
      .andWhereRaw('tags @\\? \'$[*].value \\? (@ == "like")\'')
    const likesCountQuery = likesQuery.clone().count('id')
    const likesCount = (await likesCountQuery)[0].count

    const tipsQuery = transactionsTable.clone()
      .andWhereRaw('tags @\\? \'$[*].name \\? (@ == "Category")\'')
      .andWhereRaw('tags @\\? \'$[*].value \\? (@ == "tip")\'')
    const tipsCountQuery = tipsQuery.clone().count('id')
    const tipsCount = (await tipsCountQuery)[0].count

    const likesTotalQuery = schema.clone()
      .select('*')
      .fromRaw(
        `(
          SELECT sum(quantity::bigint)
          FROM "dapp-cache".transactions
          WHERE tags @\\? \'$[*].name \\? (@ == "Category")\'
          AND   tags @\\? \'$[*].value \\? (@ == "like")\'
        ) as sum`
      )
    const likesTotal = (await likesTotalQuery)[0].sum

    const tipsTotalQuery = schema.clone()
      .select('*')
      .fromRaw(
        `(
          SELECT sum(quantity::bigint)
          FROM "dapp-cache".transactions
          WHERE tags @\\? \'$[*].name \\? (@ == "Category")\'
          AND   tags @\\? \'$[*].value \\? (@ == "tip")\'
        ) as sum`
      )
    const tipsTotal = (await tipsTotalQuery)[0].sum

    const networkFeesQuery = schema.clone()
      .select('*')
      .fromRaw(
        '(SELECT sum(reward::bigint) FROM "dapp-cache".transactions) as sum'
      )
    const networkFees = (await networkFeesQuery)[0].sum

    return {
      users,
      dappUsers,
      artists,
      publications,
      transactions,
      dappTransactions,
      likesCount,
      likesTotal,
      tipsCount,
      tipsTotal,
      networkFees
    }
  }
}
