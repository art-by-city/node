import {
  CacheOptions,
  DEFAULT_LEVEL_DB_LOCATION,
  LoggerFactory,
  Warp,
  WarpFactory
} from 'warp-contracts'

import { CONFIG } from '../../util'

export class SmartweaveAdapter {
  warp!: Warp

  constructor() {
    const cacheOptions: CacheOptions = {
      inMemory: true,
      dbLocation: DEFAULT_LEVEL_DB_LOCATION
    }

    if (CONFIG.isProduction) {
      LoggerFactory.INST.logLevel('fatal')
      this.warp = WarpFactory.forMainnet(cacheOptions)
    } else {
      LoggerFactory.INST.logLevel('error')
      this.warp = WarpFactory.forLocal(undefined, undefined, cacheOptions)
    }
  }
}
