import { injectable } from 'inversify'
import knex, { Knex } from 'knex'

import { CONFIG } from '../../util'
import { DatabaseAdapter } from './database'

@injectable()
export class KnexAdapter implements DatabaseAdapter<Knex> {
  connection!: Knex

  constructor() {
    this.connection = knex({
      client: 'pg',
      connection: {
        user: CONFIG.DB_USER,
        database: CONFIG.DB_NAME,
        password: CONFIG.DB_PASS,
        host: CONFIG.DB_HOST,
        port: CONFIG.DB_PORT
      }
    })
  }
}
