import { Exm } from '@execution-machine/sdk'

export class ExmAdapter {
  exm!: Exm

  constructor(private token: string) {
    this.exm = new Exm({ token })
  }

  createFunctionHandle(functionId: string): ExmFunction {
    return new ExmFunction(this.exm, functionId)
  }
}

export class ExmFunction {
  constructor(public exm: Exm, public functionId: string) {}

  async write(inputs: any) {
    return this.exm.functions.write(this.functionId, inputs)
  }

  async read() {
    return this.exm.functions.read(this.functionId)
  }
}
