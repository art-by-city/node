import { Knex } from 'knex'

export type DbAdapterType = Knex

export interface DatabaseAdapter<D extends DbAdapterType> {
  connection: D
}
