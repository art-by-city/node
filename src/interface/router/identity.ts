import Router from '@koa/router'

import { Context, ParameterizedContext, State } from '../../app'
import { ExmAdapter, ExmFunction } from '../../infra'
import { CONFIG } from '../../util'
import { jwt } from '../middleware/auth'
import axios, { AxiosInstance } from 'axios'

type BaseIdentityRequestPayload = {
  arweavePublicKey: string
  signature: string
}

type LinkIdentityRequestPayload = BaseIdentityRequestPayload & {
  foreignAddress: string
  network: string
  verificationReq: string
}

type UnlinkIdentityRequestPayload = BaseIdentityRequestPayload & {
  foreignAddress: string
}

type SetPrimaryAddressRequestPayload = BaseIdentityRequestPayload & {
  primary_address: string
}

export class IdentityRouter {
  router: Router<State, Context> = new Router<State, Context>()
  arkFunction!: ExmFunction
  arkApi!: AxiosInstance

  constructor(private exm: ExmAdapter) {
    this.arkFunction = exm.createFunctionHandle(CONFIG.ARK_EXM_FUNCTION_ID)
    this.arkApi = axios.create({ baseURL: 'https://ark-core.decent.land/v2' })
    this.build()
  }

  private build() {
    this.router.get('/resolve/:address', this.resolve.bind(this))
    this.router.use(jwt)
    this.router.post('/link', this.link.bind(this))
    this.router.post('/unlink', this.unlink.bind(this))
    this.router.post('/set-primary', this.setPrimary.bind(this))
  }

  private async resolve(ctx: Router.RouterContext<State, Context>) {
    try {
      const address = ctx.params.address

      const {
        data,
        status
      } = await this.arkApi.get(`/address/resolve/${address}`)

      ctx.status = status
      ctx.body = data
    } catch (error) {
      console.error('IdentityRouter.resolve() ERROR', error)

      ctx.status = 500
      ctx.body = 'Oops, couldn\'t resolve'
    }

    return
  }

  private async link(ctx: Router.RouterContext<State, Context>) {
    try {
      const {
        arweavePublicKey: jwk_n,
        foreignAddress: address,
        network,
        verificationReq,
        signature: sig
      } = ctx.request.body as LinkIdentityRequestPayload

      const inputs = [{
        function: 'linkIdentity',
        jwk_n,
        address,
        network,
        verificationReq,
        sig
      }]

      const { data } = await this.arkFunction.write(inputs)

      ctx.status = 200
      ctx.body = data
    } catch (error) {
      console.error('IdentityRouter.link() ERROR', error)

      ctx.status = 500
      ctx.body = 'Oops, couldn\'t linkIdentity'
    }

    return
  }

  private async unlink(ctx: ParameterizedContext) {
    try {
      const {
        arweavePublicKey: jwk_n,
        foreignAddress: address,
        signature: sig
      } = ctx.request.body as UnlinkIdentityRequestPayload

      const inputs = [{
        function: 'unlinkIdentity',
        jwk_n,
        address,
        sig
      }]

      const { data } = await this.arkFunction.write(inputs)

      ctx.status = 200
      ctx.body = data
    } catch (error) {
      console.error('IdentityRouter.unlink() ERROR', error)

      ctx.status = 500
      ctx.body = 'Oops, couldn\'t unlinkIdentity'
    }

    return
  }

  private async setPrimary(ctx: ParameterizedContext) {
    try {
      const {
        arweavePublicKey: jwk_n,
        signature: sig,
        primary_address
      } = ctx.request.body as SetPrimaryAddressRequestPayload

      const inputs = [{
        function: 'setPrimaryAddress',
        jwk_n,
        primary_address,
        sig
      }]

      const { data } = await this.arkFunction.write(inputs)

      ctx.status = 200
      ctx.body = data
    } catch (error) {
      console.error('IdentityRouter.setPrimary() ERROR', error)

      ctx.status = 500
      ctx.body = 'Oops, couldn\'t setPrimaryAddress'
    }

    return
  }
}