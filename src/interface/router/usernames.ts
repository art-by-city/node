import Router from '@koa/router'

import { UsernamesContractCache } from '../../infra'

export class UsernamesRouter {
  private cache!: UsernamesContractCache
  router: Router = new Router()

  constructor(cache: UsernamesContractCache) {
    this.cache = cache

    this.build()
  }

  private build() {
    this.router.get('/', async (ctx) => {
      ctx.body = await this.cache.get()

      return
    })
  }
}
