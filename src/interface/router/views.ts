import fetch from 'node-fetch'
import Koa from 'koa'
import Router from '@koa/router'

import { ViewsRepository } from '../../infra'
import { View } from '../../core/views'

type KoaContext = Koa.ParameterizedContext<
  Koa.DefaultState,
  Koa.DefaultContext & Router.RouterParamContext<
    Koa.DefaultState,
    Koa.DefaultContext
  >,
  any
>

export class ViewsRouter {
  private nodeAddress!: string
  private gateway!: string
  private repository!: ViewsRepository
  router: Router = new Router()

  constructor(
    nodeAddress: string,
    gateway: string,
    repository: ViewsRepository
  ) {
    this.nodeAddress = nodeAddress
    this.gateway = gateway
    this.repository = repository

    this.build()
  }

  private build() {
    this.router.get('/:txid', async (ctx) => {
      const ip = process.env.NODE_ENV === 'production'
        ? (ctx.headers['x-forwarded-for'] as string || '').split(',')[0]
        : ctx.request.ip
      const txid = ctx.params.txid

      const location = `${this.gateway}/${txid}`
      fetch(location).then(res => {
        if (res.ok) {
          this.repository.insert(
            new View({
              nodeAddress: this.nodeAddress,
              txid,
              type: res.headers.get('content-type') || '',
              ip
            })
          )
        }
      }).catch(err => console.error('Error requesting tx from Gateway', err))

      ctx.status = 302
      ctx.set('location', location)

      return
    })

    this.router.get('/views/:txid', async (ctx) => {
      ctx.body = await this.repository.count(ctx.params.txid)

      return
    })

    this.router.get('/views/report/:date', async (ctx) => {
      ctx.body = await this.repository.report(ctx.params.date)

      return
    })

    this.router.get('/views/trending/:date', async (ctx) => {
      ctx.body = await this.repository.trending(ctx.params.date)

      return
    })
  }
}
