import Router from '@koa/router'
import { StatsRepository } from '../../infra'

export class StatsRouter {
  private repository!: StatsRepository
  router: Router = new Router()

  constructor(repository: StatsRepository) {
    this.repository = repository

    this.build()
  }

  private build() {
    this.router.get('/summary', async (ctx) => {
      ctx.body = await this.repository.summary()

      return
    })
  }
}
