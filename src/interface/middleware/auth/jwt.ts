import jsonwebtoken from 'jsonwebtoken'
import { jwkTopem } from 'arweave/node/lib/crypto/pem'

import { ParameterizedContext } from '../../../app'

export default async function (
  ctx: ParameterizedContext,
  next: () => Promise<any>
) {
  if (ctx.headers.authorization) {
    const [ arweavePublicKey, jwt ] = ctx.headers.authorization.split(' ')

    try {
      const pem = jwkTopem({ kty: 'RSA', e: 'AQAB', n: arweavePublicKey })
      jsonwebtoken.verify(jwt, pem, {
        algorithms: ['PS256'],
        maxAge: '10m'
      })
      ctx.state.auth = { arweavePublicKey, jwt }

      await next()
    } catch (error) {
      console.error('JWT verification error', error)
      ctx.status = 401

      return
    }
  } else {
    ctx.status = 401
  }
}
