import dotenv from 'dotenv'
import type { Knex } from 'knex'

dotenv.config({ path: '../.env' })

const connection = {
  user: process.env.DB_USER || '',
  password: process.env.DB_PASS || '',
  host: process.env.DB_HOST || 'localhost',
  port: process.env.DB_PORT || 5432,
  database: process.env.DB_NAME || ''
}

const baseConfig = {
  client: 'postgresql',
  connection,
  migrations: {
    tableName: 'knex_migrations'
  }
}

const config: { [key: string]: Knex.Config } = {
  development: baseConfig,
  staging: baseConfig,
  production: baseConfig
}

module.exports = config
