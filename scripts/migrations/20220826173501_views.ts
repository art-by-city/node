import { Knex } from 'knex'

const schema =  process.env.DAPP_SCHEMA || 'artbycity'
const TABLE_NAME = 'views'

export async function up(db: Knex): Promise<void> {
  // Create dapp schema
  await db.schema.createSchemaIfNotExists(schema)

  // Views
  const exists = await db.schema.withSchema(schema).hasTable(TABLE_NAME)

  if (!exists) {
    await db.schema.withSchema(schema).createTable(TABLE_NAME, table => {
      table.string('hash', 64).notNullable().primary()
      table.string('txid', 64).notNullable()
      table.text('type').notNullable()
      table.text('date').notNullable()
      table.string('node', 64).notNullable()
      table.timestamp('timestamp').defaultTo(new Date().toISOString())

      table.index(['txid'], 'views_txid', 'HASH')
      table.index(['node'], 'views_node', 'HASH')
      table.index(['date'], 'views_date', 'HASH')
    })

    const tableWasCreated = await db.schema.withSchema(schema).hasTable(TABLE_NAME)
    if (!tableWasCreated) {
      throw new Error(`${schema}.${TABLE_NAME} was not created!`)
    }
  } else {
    throw new Error(`${schema}.${TABLE_NAME} already exists!`)
  }
}

export async function down(db: Knex): Promise<void> {
  await db.schema.withSchema(schema).dropTableIfExists(TABLE_NAME)
}

