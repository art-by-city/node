# Art By City Node

## Description
This project is a server that intercepts and redirects requests to Arweave
Gateways, tracks views of accessed content, and provides additional HTTP API endpoints to support the Art By City Protocol.

A unique view is a `SHA256` hash of:
 - `address` Arweave address of this Art By City Node
 - `ip` of requesting host
 - `txid` of requested transaction
 - `date` of access

An Art By City Node exposes a few endpoints, explained below.  These endpoints
allow for requesting the current view count of a `txid`, a daily view count
report, and a daily trending report ordered by descending view counts.  These
view counts will be used in the future to reward creators based on the
engagement of their creative content.

## Requirements
- `node` `v16.13.2`
- `postgres` `>= v9.5`

## Dev Requirements
- [ArLocal](https://github.com/textury/arlocal)

## Install
```bash
$ npm i --save @artbycity/node
```

## Environment Variables
- Postgres Config
  - `DB_HOST`
  - `DB_PORT`
  - `DB_USER`
  - `DB_PASS`
  - `DB_NAME` Database name to use

## Run Programmatically
```javascript
import ArtByCityNode from '@artbycity/node'

const abc = new ArtByCityNode('<Arweave Address of this Node>')
await abc.start()
```

Or with NuxtJS/NextJS/etc Server Middleware
```javascript
import ArtByCityNode from '@artbycity/node'

const abc = new ArtByCityNode('<Arweave Address of this Node>')
  .app
  .callback()

export default abc
```

## Rest API
- `GET /:txid` Retrieve content from Arweave via Meson Network by `txid`
  - Status `302`
  - Redirect to content on Meson Network
- `GET /views/:txid` Fetch view count for a given `txid`
  - Status `200`
  - Body `999`
- `GET /views/report/:date`
  - Fetch daily report for a given `date` (format: `M-D-YYYY` e.g. `5-31-2022`)
  - Status `200`
  - Body
  ```json
    {
      "date": "5-31-2022",
      "views": [
        {
          "nodeAddress": "EGy6I7YCKqEH8aA7IOA2HEjqeOsHqu9W4c67SFdVnmo",
          "txid": "MffAQBIB8vdKQ955CzmGObRzOMaP2_xRDUA3DqXTkt4",
          "hash": "0db4e729b1eddba1594e2bb32951393f625521ec8887cc949fba415a8d432571",
          "type": "image/png"
        },
        {
          "nodeAddress": "EGy6I7YCKqEH8aA7IOA2HEjqeOsHqu9W4c67SFdVnmo",
          "txid": "f-vKmV_9UuHgUINocW8G8YK7-jKQoUwFaI3nIOiNfc0",
          "hash": "474c65cbeedb65cd12bf7a8c7dc16cac04d8edb47176b91a2e02811afe92c200",
          "type": "image/jpeg"
        },
        {
          "nodeAddress": "EGy6I7YCKqEH8aA7IOA2HEjqeOsHqu9W4c67SFdVnmo",
          "txid": "dAzlR42uYVMkBtf9fD1KfZCc1D-cosidfvCnMVcmq3Q",
          "hash": "50cf432f83eafd9facd07ea2a968bbdd4c341310d3f4617fd7f9ae022ea18e7d",
          "type": "application/json"
        }
      ]
    }
  ```
- `GET /views/trending/:date`
  - Fetch trending list ranked by view count for a given
    `date` (format: `M-D-YYYY` e.g. `5-31-2022`)
  - Status `200`
  - Body
  ```json
    {
      "date": "5-31-2022",
      "rankings": [
        {
          "txid": "f-vKmV_9UuHgUINocW8G8YK7-jKQoUwFaI3nIOiNfc0",
          "type": "image/jpeg",
          "views": 8
        },
        {
          "txid": "dAzlR42uYVMkBtf9fD1KfZCc1D-cosidfvCnMVcmq3Q",
          "type": "application/json",
          "views": 8
        },
        {
          "txid": "MffAQBIB8vdKQ955CzmGObRzOMaP2_xRDUA3DqXTkt4",
          "type": "image/png",
          "views": 1
        }
      ]
    }
  ```

- `GET /stats/summary`
  - Fetch summary of dapp stats
  - Body
  ```json
    {
      "users":"38",
      "dappUsers":"37",
      "artists":"29",
      "publications":"139",
      "transactions":"27415",
      "dappTransactions":"405",
      "likesCount":"72",
      "likesTotal":"26400000000",
      "tipsCount":"11",
      "tipsTotal":"312800000000",
      "networkFees":"124845331418"
    }
  ```
